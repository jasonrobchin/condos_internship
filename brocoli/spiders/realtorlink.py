# -*- coding: utf-8 -*-
import scrapy
from brocoli.items import Broker

class RealtorLinkSpider(scrapy.Spider):
    name = 'rlspider'

    allowed_domains = ['realtorlink.ca']
    # start on login page
    start_urls = ['https://secure.realtorlink.ca/naflogin/naflogin.aspx?SiteDomain=mms.realtorlink.ca&targetURL=http%3a%2f%2fmms.realtorlink.ca%2f']
    # board data
    board_ids = [125]
    # board_ids = [92,65,103,69,96,17,2,105,86,23,41,66,9,20,94,32,4,97,125,88,10,123,108,122,6,121,38,89,43,44,14,57,51,11,46,37,16,12,119,48,26,70,13,54,49,85,45,5,74,118,117,114,61,50,107,15,19,98,101,76,109,110,81,31,60,83,39,52,100,25,28,77,93,115,84,95,27,64,30,8,24,78,35,47,91,53,90,82,1,3,34,7,33,36,62]
    board_urls = ['http://mms.realtorlink.ca/orgDetails.aspx?Page=1&tab=4&orgId=%d&a=All&mType=All' % bid for bid in board_ids]

    # email_image_base_url = "Presentation/ImageGenerator.aspx" DEPR
    base_url = "http://mms.realtorlink.ca"
    # current board index
    # current_board_index = int(raw_input('Current board index? (If just scraping CREA, enter 0 regardless.): '))
    current_board_index = 0
    broker_count = 0
    # start page
    start_page_num = 0
    while start_page_num < 1:
        print("Enter a page number 1 or greater.")
        start_page_num = int(raw_input('Start page?: '))
    start_page = 'http://mms.realtorlink.ca/orgDetails.aspx?Page={}&tab=4&orgId=125&a=All&mType=All'.format(start_page_num)
    first_page = True
    log_file = open('log.txt', 'a')


    # do login
    def parse(self, response):
        return scrapy.FormRequest.from_response(
            response,
            formdata={'_ctl0:MainContent:ddlBoard1': '82', '_ctl0:MainContent:InputUsername': '9531670', '_ctl0:MainContent:inputPassword': 'tarmstrong', '_ctl0:MainContent:chkRememberMe' : 'on', '_ctl0:MainContent:btnSubmit' : 'Sign in'},
            callback=self.after_login
        )

    # launch scrape
    def after_login(self, response):

        print('logged in')

	self.log_file.write("\n")
        self.log_file.write("Realtor Link Scrape Start")
        self.log_file.write("\n")

        # setup next board
        board_id = self.board_ids[self.current_board_index]
        # board_url = self.board_urls[self.current_board_index]
        board_url = self.start_page
        print("Start Page: {}".format(board_url))

        # init board data
        board_data = {'id' : board_id, 'count' : 0}

        # iterate
        self.current_board_index += 1

        yield scrapy.Request(board_url, callback=lambda r, board_data=board_data:self.parse_board_page(r, board_data))

    def parse_broker_detail(self, response, item):

        types = response.xpath('//span[re:test(@id, "indivSummary_ctlIndividualContacts_rptGrouping_lblType_\d$")]/text()').extract()
        labels = response.xpath('//span[re:test(@id, "indivSummary_ctlIndividualContacts_rptGrouping_Label\d_\d$")]/text()').extract()

        links = response.xpath('//span[re:test(@id, "indivSummary_ctlIndividualContacts_rptGrouping_lblValue_\d$")]/a/text()').extract()
        #values = response.xpath('//span[re:test(@id, "indivSummary_ctlIndividualContacts_rptGrouping_lblValue_\d$")]/text()').extract()
        values = response.xpath('//span[re:test(@id, "indivSummary_ctlIndividualContacts_rptGrouping_lblValue_\d$")]/text()').extract()

        item['_faxes'] = []
        item['_websites'] = []
        item['_emails'] = []
        item['_telephones'] = []
        item['_facebooks'] = []
        item['_twitters'] = []
        item['_linkedins'] = []

        while len(types) > 0:
            # get location (business|home) and type (telephone|fax|website|email)
            loc = labels.pop(0)
            typ = types.pop(0)

            # format key
            key = '_' + typ.lower() + ('es' if typ == 'Fax' else 's')
            cat = loc.lower()

            #print("GOT KEY %s  AND CATEGORY %s " % (key, cat))

	    try:
            	itemdict = item[key]
	    except KeyError:
		print("{} not supported.".format(key))
		continue

            arr = values
            keyval = 'url'

            # website url
            if (typ == 'Website' or typ=='Facebook' or typ=='LinkedIn' or typ=='Twitter'):
                arr = links
            # email
            elif (typ == 'Email'):
                keyval = 'email'
            # else telephone or fax
            elif (typ == 'Telephone' or typ == 'Fax'):
                keyval = 'num'
            else:
                keyval = 'val'

            # get value
            if not arr:
                continue

            val = arr.pop(0)

            # assign values
            if val:
                obj = {'location': cat}
                obj[keyval] = val
                itemdict.append(obj)


        item["languages"] = ','.join(response.xpath('//table[@id="indivSummary_grdLanguages"]/td/text()').extract())
        lang = response.xpath('//span[@id="indivSummary_lblLanguageofCorrespondance"]/text()').extract()
        item["language"] = lang[0] if lang else ""

	self.broker_count += 1
	print(self.broker_count)	

        yield item


    def parse_board_page(self, response, board_data):

        # print("CURRENT BOARD INDEX, RESUME HERE ON NEXT SCRAPE %d" % self.current_board_index)

        print("Current page: {}".format(response.url))



	self.log_file.write("Current page: {}".format(response.url))
	self.log_file.write("\n")

	referer_url = response.request.headers.get('Referer', None)

	print("Referer URL: {}".format(referer_url))

        next_page = response.xpath('//div[@class="PagingPages"]/span/a[contains(., "Next")]/@href').extract()
	# next_page = response.xpath('//div[@class="PagingPages"]/span/a[contains(., "Prev")]/@href').extract()
       
	broker_count = board_data.get('count')

        # if (next_page):
        #     print("Next Page:  %s" % next_page[0])

        brokers = response.xpath('//tr[re:test(@class, "row\d$")]')
        broker_last = len(brokers.extract()) - 1
        board_id = board_data.get('id')
        broker_count = board_data.get('count')

        # print("NEXT PAGE ? %s " % str(next_page))
        # print("LAST BROKER ? %d " % broker_last)
        # print("BOARD ID: %s  TOTAL BROKERS: %d " % (board_id, broker_count))

        for index, row in enumerate(brokers):
            broker_count += 1
            item = Broker()
            broker_id = row.xpath("@onclick").re_first(r'\'(\d*)\'')
            # print("GOT BROKER ID: %s" % broker_id)

            item["individual_id"] = broker_id
            item["board_id"] = board_id

            # populate items
            rowdata = []

            for td in row.xpath('td'):
                data = td.xpath('text()').extract()
                rowdata.append(data[0] if data else '')

            item["crea_id"] = rowdata[0]
            item["first"] = rowdata[1]
            item["last"] = rowdata[2]
            item["board_name"] = rowdata[3]
            item["role"] = rowdata[4]
            item["organization_name"] = rowdata[5]
            item["province"] = rowdata[6]

            # get detail url
            broker_url = "/indDetails.aspx?tab=0&IndividualID=%s" % broker_id
            burl = response.urljoin(broker_url)
            item['url'] = broker_url

            if index == broker_last and not next_page:
                item["final"] = broker_count
            #yield person
            yield scrapy.Request(burl, callback=lambda r, item=item:self.parse_broker_detail(r, item))

        board_data['count'] = broker_count


        # set up next url
        next_url = response.urljoin(next_page[0]) if next_page else None

        if not next_url and self.current_board_index < len(self.board_urls):
            # update next url
            next_url = self.board_urls[self.current_board_index]
            next_board_id = self.board_ids[self.current_board_index]

            # reset board data
            board_data['id'] = next_board_id
            board_data['count'] = 0

            # iterate
            self.current_board_index += 1

        if next_url:
            yield scrapy.Request(next_url, callback=lambda r, board_data=board_data:self.parse_board_page(r, board_data))
