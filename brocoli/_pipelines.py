    # -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


# class BrocoliPipeline(object):
#     def process_item(self, item, spider):
#         return item
import json
import MySQLdb

import os.path
import pytesseract
import urllib, cStringIO
from PIL import ImageFilter
import collections
import copy

from brocoli import settings

class JsonWriterPipeline(object):
    current_file_id = -1

    def __init__(self):
        print("INITIALIZED JSON WRITER PIPELINE")
        self.files = {}
        self.counts = {}
        # self.sortmap = {'id' : -1, 'board_id' : 0, 'first': 1, 'last': 2, 'email': 3, 'email_img_url': 4, 'telephone': 5, 'website': 6,
        #                 'languages': 7, 'language': 7, 'role': 8, 'province': 9, 'organization_name': 10, 'board_name' : 11, 'organization_id' : 12
        # }

    def close_spider(self, spider):
        #self.client.close()
        for name, file in self.files.iteritems():
            line_count = len(file.readlines())
            file.seek(-1, 1)
            file.write('\n\t],\n"final_count": %d\n}' % self.counts[name])
            file.close()


    def process_item(self, item, spider):
        # get board_id
        board_id = item.get('board_id', '0')
        final_count = item.get('final', 0)
        # file_name = 'brokers-%s.jl' % board_id
        file_name = 'brokers-%s.json' % board_id

        file = None

        if not file_name in self.files:
            file = open(os.path.join(settings.OUTPUT_DIR, file_name), 'w+')
            file.write('{\n"brokers": [')
            self.files[file_name] = file
            self.counts[file_name] = 0
            print("CREATED FILE !")
            print(self.files[file_name])
        else:
            file = self.files[file_name]

        od = collections.OrderedDict(sorted(item.items()))

        for k in od:
            if not od[k]:
                del od[k]

        line = "\n\t%s," % json.dumps(od)
        file.write(line)

        if final_count:
            self.counts[file_name] = final_count

        return item

class MySQLInserterPipeline(object):

    def __init__(self):
        self.db = MySQLdb.connect(host='127.0.0.1', port=8889, user='root', passwd='root', db='condos_ca')
        self.cursor = self.db.cursor()

    def close_spider(self, spider):
        self.db.close()

    def extract_values(self, key, val):
        for value in val:
            keys = ['id']
            values = []
            if key == '_emails':
                table = 'emails'
                keys.extend(['email', 'location'])
                values.append(value['email'])
                self.insert(table, keys, values)
            elif key == '_telephones':
                table = 'telephones'
                keys.extend(['number', 'location'])
                values.append(value['num'])
                self.insert(table, keys, values)
            elif key == '_websites':
                table = 'websites'
                keys.extend(['url', 'location'])
                values.append(value['url'])
                self.insert(table, keys, values)
            values.append(value['location'])

    def process_item(self, item, spider):

        agent = copy.deepcopy(item)
        print("|---------------pre agent---------------|")
        print(agent)
        print("|--------------END------------|")

        # for key, val in item.iteritems():
        #     print("|---------------KEY,VAL---------------|")
        #     print(key, val)
        #     print("|--------------KEY,VAL END------------|")
        #     keys = ['id']
        #     values = []
        #     table = 'agents'
        #     while isinstance(val, (list, dict)):
        #         val[0]

        for key, val in item.iteritems():
            if isinstance(val, (list, dict)):
                self.extract_values(key, val)
                del agent[key]
                continue

        self.insert('agents', agent.keys(), agent.values())

        print("|---------------post agent---------------|")
        print(agent)
        print("|--------------END------------|")

        return item

    def format_keys(self, keys):
        return "(" + ", ".join(keys) + ")"

    def format_values(self, values):
        vals = []
        for value in values:
            if isinstance(value, int):
                vals.append(str(value))
            else:
                vals.append(value)

        return "(\"" + "\", \"".join(vals) + "\")"

    def insert(self, table, keys, values):

        keys = self.format_keys(list(keys))
        values = self.format_values(values)

        print('keys: ' + str(keys))
        print('values: ' + str(values))

        return

        sql = "INSERT INTO `realtorcom_agents`" + keys + " VALUES " + values
        # sql = "INSERT INTO `realtorcom_agents`" + "(id, name, real_url)" + " VALUES " + "('3425', 'test', 'test')"

        print(sql)

        db = MySQLdb.connect(host='127.0.0.1', port=8889, user='root', passwd='root', db='condos_ca')
        c=db.cursor()
        c.execute(sql)
        c.execute("SELECT * FROM `realtorcom_agents` WHERE 1")
        db.commit()

class DetailDrillPipeline(object):

    def fetch_detail(self, response, item):
        telephone = response.xpath('//span[@id="indivSummary_ctlIndividualContacts_rptGrouping_lblValue_0"]/text()').extract()
        email_url = response.xpath('//input[@id="indivSummary_ctlIndividualContacts_rptGrouping_imgEmail_1"]/@src').extract()
        website = response.xpath('//span[@id="indivSummary_ctlIndividualContacts_rptGrouping_lblValue_2"]/a/text()').extract()

        if telephone:
            person['telephone'] = telephone[0]
        if email_url:
            person['email_img_url'] = response.urljoin(email_url[0])
        if website:
            person['website'] = website[0]

        print("got telephone: %s  email: %s   website: %s " % (telephone, email_url, website))

        person["languages"] = ','.join(response.xpath('//table[@id="indivSummary_grdLanguages"]/td/text()').extract())
        person["language"] = response.xpath('//span[@id="indivSummary_lblLanguageofCorrespondance"]/text()').extract()[0]

        return item

    def process_item(self, item, spider):
        url = item['url']
        yield scrapy.Request(url, callback=lambda r, item=item:self.fetch_detail(r, item))

class TeseractPipeline(object):

    def process_item(self, item, spider):

        for email in item.get('_emails'):

            email_img_url = email.get('url', '')

            if not email_img_url:
                continue

            abs_url = "%s/%s?%s" % (spider.base_url, spider.email_image_base_url, email_img_url)

            file = cStringIO.StringIO(urllib.urlopen(abs_url).read())
            img = Image.open(file).convert('RGB')
            #print(img.size)
            ratio = img.size[0]/img.size[1]
            #print('ratio %d ' % ratio)
            scale = 25
            oddy = 1
            oddx = 1

            img = img.resize((int(img.size[0]*scale * oddx), int(img.size[1]*scale * oddy)), Image.ANTIALIAS)
            img = img.filter(ImageFilter.DETAIL)
            img.save('_temp.jpg', 'jpeg', quality=100)
            email["address"] = pytesseract.image_to_string(img)
            return item
