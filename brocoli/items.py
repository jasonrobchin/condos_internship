# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class Broker(scrapy.Item):

    individual_id = scrapy.Field()
    crea_id = scrapy.Field()
    url = scrapy.Field()
    # basic broker info
    first = scrapy.Field()
    last = scrapy.Field()

    # detail info
    _emails = scrapy.Field()
    _telephones = scrapy.Field()
    _faxes = scrapy.Field()
    _websites = scrapy.Field()

    _facebooks = scrapy.Field()
    _linkedins = scrapy.Field()
    _twitters = scrapy.Field()

    # spoken languages
    language = scrapy.Field()
    languages = scrapy.Field()
    # role
    role = scrapy.Field()

    # board
    board_name = scrapy.Field()
    board_id = scrapy.Field()
    # org
    organization_name = scrapy.Field()
    organization_id = scrapy.Field()
    # locale
    province = scrapy.Field()

    # raw data
    _types = scrapy.Field()
    _labels = scrapy.Field()
    _values = scrapy.Field()
    _links = scrapy.Field()

    # final item
    final = scrapy.Field()
